﻿using UnityEngine;
using System.Collections;

public class transforms : MonoBehaviour {
    float x=0;
    public Vector3 destino;
    public float velocidad = 0.1f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //TRANSLACION.

        //instantanea
        //FORMA 1
        //transform.Translate(destino);

        //FORMA 2
        //gameObject.transform.position = destino;

        //-------------------------------------------------------------------------------

        //con transicion lenta.
        //FORMA 1
        //x += velocidad;
        //gameObject.transform.position = new Vector3(x,0,0);

        //FORMA 2
        //gameObject.transform.position = Vector3.Lerp(gameObject.transform.position,destino,velocidad*Time.deltaTime);

        //FORMA 3
        //gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, destino, velocidad * Time.deltaTime);

    }
}
