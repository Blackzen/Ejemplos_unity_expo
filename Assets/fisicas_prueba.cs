﻿using UnityEngine;
using System.Collections;

public class fisicas_prueba : MonoBehaviour {
    public Rigidbody rb;
    public float force;
	// Use this for initialization
	void Start () {
        rb = gameObject.GetComponent<Rigidbody>();
	}
    
	// Update is called once per frame
	void Update () {
        //translacion
        //rb.AddForce(Vector3.up*force);

        //rotacion.
        rb.AddTorque(Vector3.up * force);
	}
}
