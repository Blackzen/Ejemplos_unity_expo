﻿using UnityEngine;
using System.Collections;

public class Input_mouse : MonoBehaviour {
    public Color normal;

    public Color abajo;
    public Color arriba;
    public Color encima;
    public Color afuera;

    public Color apretado;
    public Color soltado;
    // Use this for initialization
    void Start () {
        normal = gameObject.GetComponent<Renderer>().material.color;
	}
	
	// Update is called once per frame
	void Update () {
        
        //TECLAS
        /*
        if (Input.GetKeyDown("space")) { 
            Debug.Log("space fue apretado");
            gameObject.GetComponent<Renderer>().material.color = apretado;
        }
        if (Input.GetKeyUp("space"))
        {
            Debug.Log("space fue soltado");
            gameObject.GetComponent<Renderer>().material.color = soltado;
        }
        */

        //MOUSE
        
        if (Input.GetMouseButtonDown(0)) {
            Debug.Log("click abajo");
            gameObject.GetComponent<Renderer>().material.color = abajo;
        }
        if (Input.GetMouseButtonUp(0)) {
            Debug.Log("click arriba");
            gameObject.GetComponent<Renderer>().material.color = arriba;
        } 

    }

    //EVENTOS DE CLICK PREDETERMINADOS.
    /*
    void OnMouseOver() {
        Debug.Log("mouse encima.");
        gameObject.GetComponent<Renderer>().material.color = encima;
    }

    void OnMouseExit()
    {
        Debug.Log("mouse salio.");
        gameObject.GetComponent<Renderer>().material.color = afuera;
    }


    void OnMouseDown()
    {
        Debug.Log("click abajo.");
        gameObject.GetComponent<Renderer>().material.color = abajo;
    }

    void OnMouseUp()
    {
        Debug.Log("click arriba.");
        gameObject.GetComponent<Renderer>().material.color = arriba;
    }
    */
}
