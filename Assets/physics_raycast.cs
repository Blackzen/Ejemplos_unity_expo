﻿using UnityEngine;
using System.Collections;

public class physics_raycast : MonoBehaviour {
    public Color active_color;

    public Color color_inicial;
	
    // Use this for initialization
	void Start () {
        color_inicial = gameObject.GetComponent<Renderer>().material.color;
    }
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.name == "Cube")
            {
                hit.collider.gameObject.GetComponent<Renderer>().material.color = active_color;
            }
            else {
                gameObject.GetComponent<Renderer>().material.color = color_inicial;
            }
            
        }
        else {
            gameObject.GetComponent<Renderer>().material.color = color_inicial;
        }
        
    }
}
